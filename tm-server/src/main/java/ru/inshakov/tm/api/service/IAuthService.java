package ru.inshakov.tm.api.service;

import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.dto.UserDTO;

public interface IAuthService {

    String getUserId();

    UserDTO getUser();

    boolean isAuth();

    void checkRoles(Role... roles);

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
