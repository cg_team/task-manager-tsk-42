package ru.inshakov.tm.api;

import ru.inshakov.tm.dto.AbstractEntityDTO;

public interface IService<E extends AbstractEntityDTO> extends IRepository<E> {

}
