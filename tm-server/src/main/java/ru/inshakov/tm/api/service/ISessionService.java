package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.dto.UserDTO;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.dto.SessionDTO;

import java.util.List;

public interface ISessionService extends IService<SessionDTO> {

    @Nullable
    SessionDTO open(@Nullable String login, @Nullable String password);

    UserDTO checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull SessionDTO session, Role role);

    void validate(@Nullable SessionDTO session);

    @Nullable
    SessionDTO sign(@Nullable SessionDTO session);

    void close(@Nullable SessionDTO session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<SessionDTO> findAllByUserId(@Nullable String userId);
}
