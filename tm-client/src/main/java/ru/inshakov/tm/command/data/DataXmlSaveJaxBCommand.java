package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

public class DataXmlSaveJaxBCommand extends AbstractCommand {

    @Nullable

    public String name() {
        return "data-save-xml-j";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Save data to XML by JaxB.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().saveDataXmlJaxB(getSession());
    }

}