package ru.inshakov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.ServiceLocator;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.exception.user.AccessDeniedException;

public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = name();
        @Nullable final String arg = arg();
        @Nullable final String description = description();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "] ";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

    @Nullable
    protected Session getSession() {
        @Nullable Session session = serviceLocator.getSession();
        if (session == null) throw new AccessDeniedException();
        return session;
    }

}
