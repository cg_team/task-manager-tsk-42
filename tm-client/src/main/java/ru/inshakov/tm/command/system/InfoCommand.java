package ru.inshakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.util.NumberUtil;

public final class InfoCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "system-info";
    }

    @NotNull
    @Override
    public String description() {
        return "System info";
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.format(freeMemory));
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.format(maxMemory);
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long memoryTotal = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.format(memoryTotal));
        final long usedMemory = memoryTotal - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));
    }

}
